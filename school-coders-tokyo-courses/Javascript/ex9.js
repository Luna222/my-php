var rectangle = {
    width: 10,
    height: 20,
    area: function(){
        return this.width * this.height;
    }
};
function getWidth(){
    return rectangle.width;
}
function getHeight(){
    return rectangle.height;
}
console.log('Width:', getWidth());
console.log('Height:', getHeight());
console.log('area:', rectangle.area());
