var rectangles = [
    {width: 10, height: 5},
    {width: 10, height: 20},
    {width: 4, height: 16}
];

var rectangleAreas = rectangles.map(function(rectangle){
    return {width: rectangle.width, height: rectangle.height, area: rectangle.width * rectangle.height};
});

console.log(rectangleAreas);

console.log('________________\n');
/**
 * 1. Viết hàm tripple nhận vào 1 số và trả về số đó nhân 3
 * 2. Sử dụng map method và hàm tripple để nhân 3 các phần tử trong mảng dưới đây
 */
// Khai báo tripple function ở đây
function tripple(num){
    return num*3;
  }
  
  var numbers = [10, 5, 8];
  // Gợi ý: numbers.map... (tự viết tiếp)
  trippleArray = numbers.map(function(element){
    return tripple(element);
  });
  
  console.log(trippleArray);

  console.log('_______________\n');

  // Make an array of strings of the names
function namesOnly(arr) {
    // for( var n of arr){
    //   console.log(n.name);
    // }
    var newArr = arr.map(n => n.name);
    console.log(newArr) ;
  }
  
  namesOnly([
    {
      name: "Angelina Jolie",
      age: 80
    },
    {
      name: "Eric Jones",
      age: 2
    },
    {
      name: "Paris Hilton",
      age: 5
    },
    {
      name: "Kayne West",
      age: 16
    },
    {
      name: "Bob Ziroll",
      age: 100
    }
  ])
  // ["Angelina Jolie", "Eric Jones", "Paris Hilton", "Kayne West", "Bob Ziroll"]
  console.log('_______________\n');
  
  // Make an array of the names in <h1></h1>, and the ages in <h2></h2>
function readyToPutInTheDOM(arr) {
    var newArr = arr.map(function(element){
      return '<h1>'+ element.name + '</h1>' + '<h2>'+ element.age + '</h2>';
    });
    console.log(newArr);
  }
  readyToPutInTheDOM([
    {
      name: "Angelina Jolie",
      age: 80
    },
    {
      name: "Eric Jones",
      age: 2
    },
    {
      name: "Paris Hilton",
      age: 5
    },
    {
      name: "Kayne West",
      age: 16
    },
    {
      name: "Bob Ziroll",
      age: 100
    }
  ]) 
  // ["<h1>Angelina Jolie</h1><h2>80</h2>", 
  // "<h1>Eric Jones</h1><h2>2</h2>", 
  // "<h1>Paris Hilton</h1><h2>5</h2>", 
  // "<h1>Kayne West</h1><h2>16</h2>", 
  // "<h1>Bob Ziroll</h1><h2>100</h2>"]
