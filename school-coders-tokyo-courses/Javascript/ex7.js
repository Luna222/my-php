var a = 10;

/**
 * Viết ra tất cả các cách khác nhau có thể (trong phạm vi bài học) để tăng a lên 2 đơn vị
 */
a += 2;
console.log(a);
//temp a = 12
a = a + 2;
console.log(a);

//______________________

var a1 = 10;

/**
 * Viết ra tất cả các cách khác nhau có thể (trong phạm vi bài học) để gấp đôi a
 */

 a1 *= 2;
 console.log(a1);
 //temp a1 = 20;
 a1 = a1 * 2;
 console.log(a1);