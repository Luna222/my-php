var myCat = {
    name: 'kaito',
    age: '3 months',
    isAlive: false
}

var me = {
    name: 'Taki',
    age: '22',
    isHappy: false
}

myCat.isAlive = true;
me['isHappy'] = true;

console.log(myCat);
console.log(me);