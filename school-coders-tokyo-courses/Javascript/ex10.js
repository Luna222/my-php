/**
 * Dùng vòng lặp for hiển thị ra màn hình các số từ 0 đến 9
 *//**
 * Dùng vòng lặp for hiển thị ra màn hình các số từ 0 đến 9
 */
for(var i = 0; i < 10; i++){
    console.log(i);
}
/**
 * Dùng vòng lặp for để hiển thị ra màn hình bảng chữ cái abc.
 * Gợi ý: Dùng String.fromCharCode
 * Ví dụ: console.log(String.fromCharCode(97)) sẽ hiển thị ra màn hình chữ "a" 
 * Tham khảo: http://www.asciitable.com/ để hiểu thêm về mã ascii
 */
for(i = 97; i < 122; i++){
    console.log(String.fromCharCode(i));
}
var a = [1, 2, 4, 8, 16];
/**
 * Sử dụng vòng lặp for để hiển thị ra màn hình các phần tử của mảng a theo thứ tự ngược lại:
 * 16
 * 8
 * 4
 * 2
 * 1
 */

for(i = a.length - 1; i >= 0; i--){
    console.log(a[i]);
}
console.log("=============")
/**
 * Viết function trả về tích các số từ start đến end, không tính end 
 * Ví dụ: console.log(calculate(2, 5)) sẽ hiển thị ra màn hình 24 (vì 2 * 3 * 4 = 24)
 */
var result = 1;
function calculate(start, end) {
  for(var i = start; i < end; i++){
    
    result = result * i;
  }
  return result;
}
console.log(calculate(4,5));

console.log("===============")

var a = [1, 2, 3, 4, 5];
var b = [10, 20, 30];
/**
 * Sử dụng vòng lặp for để tính tích của các phần tử của array a với các phần tử của array b. Kết quả mong muốn:
 * 10
 * 20
 * 30
 * ...
 * 20
 * 40
 * 60
 * ...
 * Gợi ý: Dùng nested for loops (các vòng lặp lồng nhau)
 */
for(var i = 0; i < a.length; i++){
  for(var j = 0; j < b.length; j++){
    console.log(a[i] * b[j]);
  }
}


