/**
 * Cách sử dụng Array và Object khác nhau như thế nào. Viết câu trả lời ở phía dưới.
 */
//Array luu mot list cac elements khac nhau
//Object dung de mo ta 1 vat the co nh thuoc tinh khac nhau

var studentList = [
    {name: 'Taki', age: 22, gender: 'pansexual', job: 'senior IT student'},
    {name: 'Emma', age: 23, gender: 'female', job: 'philosophy teacher'},
    {name: 'Hung', age: 24, gender: 'male', job: 'marketer'}
];

console.log(studentList);