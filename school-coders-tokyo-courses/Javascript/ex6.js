var a = 10;

var x = --a + a++;
//x = 9 + 9 (temp a = 10);
console.log(x);
console.log(a);

// Nhấn Run để chạy code.
// Giải thích vì sao kết quả là 18.

var b = 10;
var c = 18;

var y = b-- + c++ - ++c - ++b;
//y = 10 + 18 - ++c - ++b (temp b = 10, temp c = 20 )
//y = 10 + 18 - 10 - 20
//y = -2
console.log(y);
