/**
 * 1. Viết hàm sayHello làm nhiệm vụ log ra màn hình 'I love Coders.Tokyo'
 * 2. Viết hàm countAndDo nhận vào tham số là 1 hàm. Hàm này log ra màn hình từ 1 đến 10 dùng vòng lặp for. Sau khi log xong, chạy hàm được truyền vào ở tham số
 * 3. Kiểm tra 2 hàm trên bằng cách: 
 *   countAndDo(sayHello)
 */

function sayHello() {
    console.log('I love Coders.Tokyo');
  };
  
  function countAndDo(callBack) {
    for(var i = 1; i <= 10; i++){
      console.log(i);
    }
    callBack();
  };
  
  countAndDo(sayHello);

  console.log('_____________________')

  /**
 * 1. Viết hàm double nhận vào 1 số bất kì, hiển thị ra số đó nhân đôi
 * 2. Viết hàm sumAndDo nhận vào 1 array và 1 callback function. Hàm sumAndDo làm nhiệm vụ tính tổng array đó, sau đó gọi callback function với tham số là kết quả tổng vừa tính
 */

function double(num) {
    num *= 2;
    return num;
  };
  
  function sumAndDo(nums, callback) {
    var reducer = (a,b) => a + b;
    var sum = nums.reduce(reducer, 0);
    console.log(sum);
    callback(sum);
  };
  
  sumAndDo([1, 2, 3], double);

  console.log('________________________')
  /**
 * 1. Viết hàm transform nhận vào 1 array gồm các số và 1 function callback. Nội dung của hàm bao gồm:
 * - Khai báo 1 biến result có giá trị là 1 empty array
 * - Lặp qua từng phần tử trong array, với mỗi phần tử, gọi hàm callback và truyền vào phần tử đó, được kết quả bao nhiêu thì push vào array khai báo ở trên
 * - Trả về result array
 */
function transform(numbers, callback) {
    // Viết nội dung hàm ở đây 
      var result = [];
      for(var i = 0; i < numbers.length; i++){
        numbers[i] = callback(numbers[i]);
        result.push(numbers[i]);
      }
      return result;
    }
    
    function double(num) {
      return num * 2;
    }
    
    var output = transform([2, 4, 6], double);
    console.log(output);
    // Expect: [4, 8, 12]